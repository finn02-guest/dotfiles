import System.Taffybar.Support.PagerHints (pagerHints)

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.ManageHelpers (composeOne, (-?>))
import XMonad.Hooks.FadeInactive (setOpacity)

import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Util.NamedScratchpad (namedScratchpadAction, NamedScratchpad(NS), customFloating, namedScratchpadManageHook)

import XMonad.Layout.BinarySpacePartition (emptyBSP, ResizeDirectional(ExpandTowards, ShrinkFrom), Rotate(Rotate), Swap(Swap), FocusParent(FocusParent), SelectMoveNode(SelectNode, MoveNode))
import XMonad.Layout.Grid (Grid(..))
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders (noBorders,smartBorders)
import XMonad.Layout.Reflect (reflectHoriz)
import XMonad.Layout.ResizableTile (ResizableTall(..))
import XMonad.Layout.SimpleFloat (simpleFloat)

import Data.Default (def)
import XMonad.Prompt (XPConfig(..))
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Ssh (sshPrompt)
import XMonad.Prompt.Theme (themePrompt)
import XMonad.Prompt.Window (windowPromptBring, windowPromptGoto)
import XMonad.Prompt.XMonad (xmonadPrompt)

import XMonad.Actions.CopyWindow (copyToAll,killAllOtherCopies)
import XMonad.Actions.CycleWS (nextWS, prevWS)

import qualified XMonad.StackSet as W

import System.IO

myManageHook = composeOne
    [
      className =? "Evince"     -?> doFloat
    , className =? "Xpdf"       -?> doFloat
    , className =? "Epdfview"   -?> doFloat
    , className =? "Gimp"       -?> doFloat
    , className =? "Vncviewer"  -?> doFloat
    , className =? "gm display" -?> doFloat
    , className =? "mpv"        -?> doFloat
    ] <+> myNSManageHook scratchpads

myTerminal :: String
myTerminal = "urxvt"

myLayoutHook  = others
    where
        tiled = (ResizableTall 1 (2/100) (1/2) [])
        others = mkToggle (single MIRROR) (smartBorders (emptyBSP ||| tiled ||| Grid ||| Full ||| simpleFloat))

modm, altMask, ctrlMask :: KeyMask
modm = mod4Mask
altMask = mod1Mask
ctrlMask = controlMask

xpc = def { font = "xft:Hack:pixelsize=28,10x20bold"
          , height = 36
}

myConfig = def {
         modMask = modm
       , terminal = myTerminal
       , normalBorderColor = "#111111"
       , focusedBorderColor = "#333333"
       , manageHook = manageHook def <+> myManageHook
       , layoutHook = avoidStruts $ myLayoutHook
    } `additionalKeys` [
    ((0                 , 0x1008ff12 ), spawn "pactl set-sink-mute 1 toggle" )
  , ((modm .|. shiftMask, xK_z), spawn "slock")
  , ((modm, xK_v ), windows copyToAll) -- @@ Make focused window always visible
  , ((modm .|. shiftMask, xK_v ),  killAllOtherCopies) -- @@ Toggle window state back
  , ((modm              , xK_F12   ), xmonadPrompt      xpc                 )
  , ((modm              , xK_r     ), shellPrompt       xpc                 )
  , ((modm              , xK_F4    ), sshPrompt         xpc                 )
  , ((modm              , xK_F5    ), themePrompt       xpc                 )
  , ((modm              , xK_F6    ), windowPromptGoto  xpc                 )
  , ((modm              , xK_F7    ), windowPromptBring xpc                 )
  , ((modm              , xK_comma ), prevWS                                )
  , ((modm              , xK_period), nextWS                                )
  , ((modm              , xK_c     ), kill                                  )
  , ((modm              , xK_x     ), sendMessage $ Toggle MIRROR           )
  , ((modm              , xK_b     ), sendMessage ToggleStruts              )

  , ((modm .|. altMask,               xK_l     ), sendMessage $ ExpandTowards R)
  , ((modm .|. altMask,               xK_h     ), sendMessage $ ExpandTowards L)
  , ((modm .|. altMask,               xK_j     ), sendMessage $ ExpandTowards D)
  , ((modm .|. altMask,               xK_k     ), sendMessage $ ExpandTowards U)
  , ((modm .|. altMask .|. ctrlMask , xK_l     ), sendMessage $ ShrinkFrom R)
  , ((modm .|. altMask .|. ctrlMask , xK_h     ), sendMessage $ ShrinkFrom L)
  , ((modm .|. altMask .|. ctrlMask , xK_j     ), sendMessage $ ShrinkFrom D)
  , ((modm .|. altMask .|. ctrlMask , xK_k     ), sendMessage $ ShrinkFrom U)
  , ((modm .|. shiftMask,             xK_r     ), sendMessage Rotate)
  , ((modm,                           xK_s     ), sendMessage Swap)
  , ((modm,                           xK_n     ), sendMessage FocusParent)
  , ((modm .|. ctrlMask,              xK_n     ), sendMessage SelectNode)
  , ((modm .|. shiftMask,             xK_n     ), sendMessage MoveNode)

  , ((modm .|. controlMask, xK_m     ), namedScratchpadAction scratchpads "music")
  , ((modm .|. controlMask, xK_e   ), namedScratchpadAction scratchpads "emacs")
      ]

scratchpads :: [NamedScratchpad]
scratchpads =
    [ NS "music" "urxvt -title Music -e ncmpcpp"
             (title =? "Music")
             (customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3))
    , NS "emacs" "emacs"
             (className =? "Emacs")
             (customFloating $ W.RationalRect (5/8) (0) (3/8) (2/3))
    ]

myNSManageHook :: [NamedScratchpad] -> ManageHook
myNSManageHook s =
    namedScratchpadManageHook s
    <+> composeOne
            [ title =? "Music"
              -?> (ask >>= \w -> liftX (setOpacity w 0.7) >> idHook)
            , className =? "Emacs"
              -?> (ask >>= \w -> liftX (setOpacity w 0.8) >> idHook)
            ]

main = xmonad . withUrgencyHook NoUrgencyHook . docks . ewmh . pagerHints $ myConfig
