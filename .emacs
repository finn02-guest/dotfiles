;; This should be UNNECESSARY
(add-to-list 'load-path "/usr/share/org-mode/lisp")

(require 'org-install)
(require 'org-notmuch)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(define-key global-map "\C-cc" 'org-capture)

;; 2 capture templates for TODO tasks, and Notes
     (setq org-capture-templates
      '(("t" "Email Todo" entry (file+headline "~/org/sflc.org" "Unfiled")
             "* TODO %?  :email:\n  %T\n %i\n  [[notmuch-search:id:%:message-id][Email %:subject]] \n  From: %:from")))

(setq org-log-done t)
(setq org-icalendar-store-UID t)

;; (setq org-agenda-files (list "~/org/sflc.org"
;;                              "~/org/personal.org"))

(setq org-agenda-files (file-expand-wildcards "~/org/*.org"))

(setq org-export-select-tags (list "sflc"))

(require 'notmuch)

(setq notmuch-folders '(("inbox" . "tag:inbox")
            ("unread" . "tag:inbox AND tag:unread")
            ("firm" . "tag:inbox AND to:firm@softwarefreedom.org")
            ("2010" . "tag:archive AND tag:2010")
            ("SPAM" . "tag:spam")))

(defun notmuch-go-to-inbox()
  "search: inbox and not deleted"
  (interactive)
  (notmuch-search "tag:inbox and not tag:deleted" t))
(define-key notmuch-search-mode-map "I" 'notmuch-go-to-inbox)

(define-key notmuch-search-mode-map "d"
  (lambda ()
    "delete thread and advance"
    (interactive)
    (notmuch-search-tag "+deleted")
    (forward-line)))

(define-key notmuch-show-mode-map "d"
  (lambda ()
  "delete current message and advance"
  (interactive)
  (notmuch-show-tag-message "+deleted")
  (notmuch-show-next-open-message)))

(define-key notmuch-search-mode-map "u"
  (lambda ()
    "undelete thread and advance"
    (interactive)
    (notmuch-search-tag "-deleted")
    (forward-line)))

(define-key notmuch-show-mode-map "u"
  (lambda ()
  "undelete current message and advance"
  (interactive)
  (notmuch-show-tag-message "-deleted")
  (notmuch-show-next-open-message)))

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(message-directory "~/mail/")
 '(notmuch-fcc-dirs (quote ~/mail/sent))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
)
