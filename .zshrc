fpath=(~/.zsh/functions $fpath)
# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh/history
HISTSIZE=8192
SAVEHIST=8192
export LANG=en_US.UTF-8
setopt appendhistory extendedglob notify histignorealldups sharehistory correctall
setopt extendedhistory
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install

source ~/.zsh/completion
source ~/.zsh/prompt
