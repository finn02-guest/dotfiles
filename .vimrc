syntax enable
set background=light
colorscheme solarized
set showcmd
set autoindent smartindent

:highlight TrailWhitespace ctermbg=red guibg=red
:match TrailWhitespace /\s\+$\| \+\ze\t/

:autocmd Syntax * syn match TrailWhitespace /\s\+$\| \+\ze\t/
filetype plugin indent on

au FileType haskell setlocal modeline expandtab
au FileType python setlocal tabstop=8 expandtab shiftwidth=4 softtabstop=4

if !has("gui_running")
  set mouse=
endif
